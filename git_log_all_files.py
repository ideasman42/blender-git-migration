#!/usr/bin/env python3

"""
This script makes a log of all files that were every in git across all branches.

Good for finding files to prune from conversion.
"""

import os, collections

OUT_NAME = "out.txt"

print("Creating Log...")
os.system("git log --all --pretty=format: --name-only --diff-filter=A | sort | uniq > %s" % OUT_NAME)

print("Parsing Log...")
d = collections.defaultdict(list)
with open(OUT_NAME, "r") as f:
    for l in f:
        try:
            e = l.rsplit(".", 1)[1]
        except IndexError:
            e = ""
        d[e].append(l)

print("Done!")

for e, v in sorted(d.items()):
   print("\n\n[ %s ]\n\n" % e)
   for l in v: print(l)

os.remove(OUT_NAME)
