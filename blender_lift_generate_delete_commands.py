#!/usr/bin/env python2

# This script simply outputs a big regex string to pass into svncutter's `expunge` argument
# the reason we use this is it means we can comment each branch and group them usefully
#
# called from 'run_svndump_svncutter.sh'

import re

args = []

# ------------------------------------------------------------------------------
# Branches
#
# note: /USEGIT suffix means we should use git to delete the branches
branches = [
    "soc-2007-hcube",  #  never merged (sound API, we ended up going with Audaspace)
    "soc-2007-mosani",  # never merged (render api)
    "soc-2007-maike",  # never merged (glsl)
    "soc-2007-joeedh",  # never merged (deep shadow maps)
    "soc-2007-red_fox",  # bevel, since rewritten
    "soc-2008-unclezeiv",  # never merged (light-cuts)
    "soc-2008-djd",  # never merged (unit-testing only 17 commits)
    "soc-2009-yukishiro",  # image based lighting
    "soc-2010-aligorith-2",  # never merged (physics - ended up re-branching and working on later)
    "soc-2010-kwk",  # never merged (paint system, only ~20 commits)
    "soc-2010-leifandersen",  # never merged (testing project)
    "soc-2010-moguri",
    "soc-2010-moguri-2",
    "soc-2010-rohith291991",  # never merged (quad dominant mesh)
    "soc-2011-avocado",  # never merged (mesh tools, very few commits)
    "soc-2011-salad",  # salad is a mix, no need to merge.
    "soc-2011-oregano",  # almost no commits
    "soc-2011-onion",  # never merged (ptex)
    "soc-2011-onion-uv-tools",  # branch is deleted in svn
    "soc-2011-sven",  # never merged (from nodelogic branch, never committed to).
    "soc-2013-vse",  # wrote new sequencer engine - TODO, check if it will be used.
    "soc-2013-cycles_mblur",  # unfinished
    "soc-2013-meshdata_transfer",  # development can continue externally (rewrote started after gsoc finishes)
    "soc-2013-gl_debug",  # has only 3 commits.
    "2-44-stable/USEGIT",  # use tags instead.
    "2.44-stable/USEGIT",  # use tags instead.
    "BLENDER_INTERN",  # branch made right after initial commit, never committed to.
    "BLENDER_PROJECTFILES",  # branch only contains msvc project files.
    "animsys-aligorith",  # we've merged animsys-aligorith2
    "asset-browser",  # looks like large parts will be re-written - perhaps rebranch once the git move is done.
    "bb_dev",  # never merged
    "bb_dev_25",  # never merged
    "bge_eigen2",  # never merged
    "blender-2.62-editmesh",  # was more of a tag incase bmesh merge failed.
    "blender-autokey",  # created accidentally
    "bmesh_tmp",  # temp branch
    "branch-farsthary",  # never merged (only 10 commits, unlimited clay)
    "carve_booleans",  # not very much useful history here, mainly just getting carve lib setup.
    "cloth-eltopo",  # never merged
    "game_engine/USEGIT",  # merged but only ever had 4 useful commits. remove via git so history isnt lost.
    "ge_candy",  # this branch has some great stuff but looks like no near term plans to merge into master, could continue in external repo.
    "ge_components",  # never merged
    "ge_dyn_load",  # we DID merge, but very few commits, not useful history
    "ge_harmony",  # never merged
    "ge_integration",  # never merged
    "ge_nodelogic",  # never merged
    "hairsim",  # never merged (eltopo)
    "hive",  # experimental?, never merged.
    "imgbrowser-elubie",  # 
    "itasc",  # we DID merge this but branch was removed from SVN and history is mostly in a single commit.
    "merwin-playground",  # never merged
    "merwin-tablet",  # never merged
    "merwin-tablet-2",  # never merged
    "multiview",  # only dumps of git branch, no useful history
    "nurbs",  # never merged
    "nurbs-merge",  # never merged
    "nurbs25",  # never merged
    "particles",  # also never merged
    "particles-2010",  # never merged
    "physics25",  # never merged
    "pyapi_devel",  # experimental, never merged
    "pyapi_devel_26",  # experimental, never merged
    "pynodes",  # includes LUA api, we dont use these now.
    "qdune",  # experimental, never merged
    "sound-branch",  # an update of soc-2007-hcube
    "web-plugin",  # never merged (only 4 commits)
    ]


for b in branches:
    args.append("^branches/" + re.escape(b) + "(/|$)")


# ------------------------------------------------------------------------------
# Tags
#

tags = [
    "lib2.4",
    "2-44-stable-root",
    "BF_TO_ORANGE",
    "BLENDER-root",
    "PREMERGE_2006_01_28",
    "PRE_ORANGE_2006_01_28",
    "blender-2-47-release",
    "blender-2.47-root",
    "blender2.5-root",
    "bpy-cleanup-20040925",
    "bpy-cleanup-pre-20041007",
    "imgbrowser-elubie-root",
    "mem_leak-1",
    "qdune-root",
    "seqaudio-postmerge",
    "seqaudio-premerge",
    "hairsim-root",
    "hairsim-root",
    "soc-2009-imbusy-root",
    "soc-2011-avocado-root",
    ]

for b in tags:
    args.append("^tags/" + re.escape(b) + "(/|$)")

# -----
# Other
#

# sometimes we accidentally made a branch of lib
args.append("^branches/[^/]+/lib(/|$)")

args.append("^trunk/lib(/|$)")

if 0:
    print("|".join(args))

if 1:
    print("#")
    for b in branches:
        if not b.endswith("/USEGIT"):
            print("branch %s delete" % b)
    print("#")
    for b in branches:
        if b.endswith("/USEGIT"):
            print("git branch -D %s" % b.split("/")[0])
    print("#")
    for b in tags:
        print("git tag -d %s" % b)

