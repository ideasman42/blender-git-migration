#!/usr/bin/env python3

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

"""
This script helps answer the question:
Which was the last revision that modified trunk?

Merge commands often contain numbers which don't represent
the number of a commit to trunk (or the branch in question).

eg:
  svn_find_last_path.py /trunk/blender 123

  ... assuming 123 is a commit to a branch,
  this will find the last commit which touched trunk.
"""

import sys

path, rev = sys.argv[-2:]
rev = int(rev)

def run(cmd):
    # print(">>> ", " ".join(cmd))
    import subprocess
    proc = subprocess.Popen(" ".join(cmd),
                            shell=True,
                            stderr=subprocess.PIPE,
                            stdout=subprocess.PIPE)

    result = proc.stdout.read()
    return result

while 1:
    cmd = [
        "svn",
        "log",
        # "file:///svnroot/bf-blender",  # XXX
        "^/",  # all branches/tags/trunk!
        "-c%r" % rev,
        "--verbose",
        "--xml",
        ]

    xml = run(cmd)

    found = False

    from xml.dom.minidom import parseString
    tree = parseString(xml)
    log = tree.getElementsByTagName("log")[0]
    commits = log.getElementsByTagName("logentry")
    # will only be one commit
    ci = commits[0]
    paths = ci.getElementsByTagName("paths")[0]
    for p in paths.getElementsByTagName("path"):
        fn = p.childNodes[0].nodeValue

        if path in fn:
            found = True
            break

    if found:
        break
    else:
        rev -= 1

print(rev)


