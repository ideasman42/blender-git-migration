#!/bin/sh

# create an optimized bare repo,
# run after 'convert.sh'

git clone --bare file://$PWD/git git_opti
cd git_opti

git -c gc.reflogExpire=0 -c gc.reflogExpireUnreachable=0 -c gc.rerereresolved=0 \
    -c gc.rerereunresolved=0 -c gc.pruneExpire=now \
    gc --aggressive

cd ..
echo "Repo size:" $(du -h git_opti)

