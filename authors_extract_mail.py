# -*- coding: utf-8 -*-
import os
TMP_TXT = "tmp.txt"

f_in = open("authors.txt", encoding="utf-8")
f_out = open("authors.txt.new", 'w', encoding="utf-8")


# copied from authors.py
def extract_author(l):
    vals = l.split()
    user = vals.pop(0)
    mail = vals.pop(-1)
    assert(vals.pop(0) == "=")
    name = " ".join(vals)
    return user, name, mail


for l in f_in.readlines():
    l = l.strip()
    if l:
        user, name, mail = extract_author(l)
        if "@" in mail:
            addr = mail.strip("<>")
        else:
            url = "http://projects.blender.org/users/%s" % user
            cmd = ("wget", url, "-O", TMP_TXT)
            print(cmd)
            os.system(" ".join(cmd))

            f_tmp = open(TMP_TXT, encoding="utf-8")
            l_tmp_prev = ""
            for l_tmp in f_tmp.readlines():
                if "<td>Real name </td>" in l_tmp_prev:
                    name = l_tmp.replace("<td><strong>", "").strip().split("</")[0].strip()

                if "@nospam@" in l_tmp:
                    a, b = l_tmp.split("@nospam@")
                    a = a[a.rindex(">") + 1:].strip()
                    b = b[:b.index("<")].strip()
                    addr = "%s@%s" % (a, b)
                    print("Email for %r is %r" % (user, addr))
                    break
                l_tmp_prev = l_tmp
            else:
                print("Email for %r not found" % user)
                addr = None
            f_tmp.close()

        f_out.write("%s = %s <%s>\n" % (user, name, addr))
        f_out.flush()

f_in.close()
